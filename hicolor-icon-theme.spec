Name:             hicolor-icon-theme
Version:          0.18
Release:          1
Summary:          The default icon theme

License:          GPL-2.0-or-later
URL:              https://www.freedesktop.org/wiki/Software/icon-theme/
Source0:          https://icon-theme.freedesktop.org/releases/%{name}-%{version}.tar.xz
BuildArch:        noarch
BuildRequires:    meson >= 0.60

%description
Icon-theme contains the standard also references the default
icon theme called hicolor.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson
%meson_build

%install
%meson_install
touch %{buildroot}%{_datadir}/icons/hicolor/icon-theme.cache

%transfiletriggerin -- %{_datadir}/icons/hicolor
gtk-update-icon-cache --force %{_datadir}/icons/hicolor &>/dev/null || :

%transfiletriggerpostun -- %{_datadir}/icons/hicolor
gtk-update-icon-cache --force %{_datadir}/icons/hicolor &>/dev/null || :

%files
%doc README.md NEWS
%license COPYING
%dir %{_datadir}/icons/hicolor
%{_datadir}/icons/hicolor/128x128
%{_datadir}/icons/hicolor/128x128@2
%{_datadir}/icons/hicolor/16x16
%{_datadir}/icons/hicolor/16x16@2
%{_datadir}/icons/hicolor/192x192
%{_datadir}/icons/hicolor/192x192@2
%{_datadir}/icons/hicolor/22x22
%{_datadir}/icons/hicolor/22x22@2
%{_datadir}/icons/hicolor/24x24
%{_datadir}/icons/hicolor/24x24@2
%{_datadir}/icons/hicolor/256x256
%{_datadir}/icons/hicolor/256x256@2
%{_datadir}/icons/hicolor/32x32
%{_datadir}/icons/hicolor/32x32@2
%{_datadir}/icons/hicolor/36x36
%{_datadir}/icons/hicolor/36x36@2
%{_datadir}/icons/hicolor/48x48
%{_datadir}/icons/hicolor/48x48@2
%{_datadir}/icons/hicolor/512x512
%{_datadir}/icons/hicolor/512x512@2
%{_datadir}/icons/hicolor/64x64
%{_datadir}/icons/hicolor/64x64@2
%{_datadir}/icons/hicolor/72x72
%{_datadir}/icons/hicolor/72x72@2
%{_datadir}/icons/hicolor/96x96
%{_datadir}/icons/hicolor/96x96@2
%{_datadir}/icons/hicolor/scalable
%{_datadir}/icons/hicolor/symbolic
%{_datadir}/icons/hicolor/index.theme
%ghost %{_datadir}/icons/hicolor/icon-theme.cache
%{_datadir}/pkgconfig/default-icon-theme.pc

%changelog
* Wed Nov 06 2024 Funda Wang <fundawang@yeah.net> - 0.18-1
- update to 0.18

* Tue Oct 25 2022 wangkerong <wangkerong@h-partners.com> - 0.17-7
- rebuild for next release

* Tue Feb 9 2021 jinzhimin <jinzhimin2@huawei.com> - 0.17-6
- rebuild hicolor-icon-theme

* Tue Feb 9 2021 jinzhimin <jinzhimin2@huawei.com> - 0.17.5
- fix the bug for packing files

* Mon Aug 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.17-4
- Package Init
